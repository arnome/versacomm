from setuptools import setup

setup(name='VersatileAmpliconSequencingAnalyses',
      version='0.1',
      description='VersatileAmpliconSequencingAnalyses (versacomm) was designed as a versatile jupyter notebook for analysing Illumina high-throughput amplicon sequencing data.',
      url='https://framagit.org/arnome/versacomm',
      author='Arnaud Mounier',
      author_email='arnaud.mounier@inra.fr',
      license='CECILL',

      packages=['versacomm'],
      zip_safe=False,
      include_package_data=True)
