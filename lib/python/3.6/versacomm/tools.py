"""
Global module manage all lobal settings for the versacomm project
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import re
import pathlib
import yaml
from configparser import ConfigParser, ExtendedInterpolation
from datetime import datetime

# Get local path
here = pathlib.Path(__file__).resolve()
lib_path = here.parent
# Add local path
sys.path.insert(0,"{0:s}".format(str(lib_path)))

# Import local librairies
from manage_sci_project import manage_project as mp
from manage_sci_project import manage_shell_cmd as msp

trim_phred_pattern = re.compile(r".*phred(?P<PHRED>\d+).*")
trim_data_pattern = re.compile(r"(?P<VAR>[a-zA-Z\s]+):\s(?P<VAL>\d+)\s")

def get_pear_output_data(pear_output_file):
    '''Read the PEAR out file get :        
        - assembled reads (size and percent)
        - discarded reads (size and percent)
        - not assembled reads (size and percent)
        and return a dict : 
          {Assembled_reads: {total_size: %d, size: %d, percent: %f},
           Discarded_reads: {total_size: %d, size: %d, percent: %f},
           Not_assembled_reads: {total_size: %s, size: %s, percent: %s}}'''
    dictOfPearOutpouts = {}
    pattern = re.compile(r"^(?P<KEY>[a-zA-Z0-9_\s]+)\s\.*: (?P<SIZE>[0-9,]+)\s/\s(?P<TOTAL>[0-9,]+)\s\((?P<PERCENT>[0-9.]+)%\).*", re.IGNORECASE)
    if pathlib.Path(pear_output_file).exists():
        with open(pear_output_file,"r") as fh:
            for line in fh.readlines():
                m = pattern.match(line)
                if m:
                    key = m.group('KEY')
                    key = key.replace(" ","_")
                    dictOfPearOutpouts[key] = {}
                    dictOfPearOutpouts[key]["total_size"] = int(m.group('TOTAL').replace(",",""))
                    dictOfPearOutpouts[key]["size"] = int(m.group('SIZE').replace(",",""))
                    dictOfPearOutpouts[key]["percent"] = float(m.group('PERCENT'))
        fh.close()
    else:
        raise ValueError("File not found")
    return dictOfPearOutpouts


def get_data_from_trim_output(trim_output):
    if not trim_output:
        raise ValueError("No trim data output")
    else:
        data = {}
        for i,line in enumerate(trim_output.split("\n")):
            if i == 2:
                m = trim_phred_pattern.match(line.strip())
                if m:
                    data['phred'] = m.group("PHRED")
                else:
                    raise ValueError("No data for phred on line",line)
            elif i == 3:
                variables = trim_data_pattern.findall(line)
                if variables:
                    for (k,v) in variables:
                        data[k.replace(" ","_")] = v
                else:
                    raise ValueError("No data for phred on line",line)
            
    return data