"""
Module manage_project purpose is to create a wide scientific project or a short
scientific project, it can create and maintain experiment's path as well as sessions
within experiments.
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import pathlib
import yaml
from configparser import ConfigParser, ExtendedInterpolation
from datetime import datetime

class Session:
    """Class session store and write the session part"""
    def __init__(self, date=None, name = None):
        if not date:
            date = set_default_moment()
        if not name:
            name = ''
        else:
            name = "_" + name
        self.name = date + name
        self.tree = False

    def set_location_dir(self, path = None, dir = None):
        """get_location_dir return the path join to the root dir of the project and created the directory if not exist. return a str"""
        if path:
            p = pathlib.Path(path)
            # if p.parts[0] == self.name the path start with the project name so just take the path of the project
            if p.parts[0] == self.name:
                p = self.path / p
            elif p.parts[0] == '/':
                raise ValueError("Path can't start with / : {0:s}".format(str(p)))
            else:
                p = self.path / self.name / pathlib.Path(path)
            if not p.exists():
                p.mkdir()
        else:
            p = self.path / self.name
        if dir:
                p = p / dir
                if not p.exists():
                    p.mkdir()
        return str(p)

    def set_location_file(self, file_name, path = None):
        """get_location_file return the path join to the file name, create the directory if not exist and return a string"""
        p = self.set_location_dir(path)
        file_path = pathlib.PurePath(p).joinpath(file_name)
        return str(file_path)

    def get_name(self):
        return(self.name)

    def get_str_path(self):
        return(str(self.path))

    def create(self,exp,exp_cfg,exp_cfg_file):
        self.path = pathlib.Path(exp.set_location_dir(exp_cfg['Sessions']['path']))
        if self.name in exp_cfg.sections():
            self.tree = True
            print("Session {} already exist, just read the conf file.".format(self.get_name()))
            return False
        else:
            self.set_tree()
            self.set_config_file()
            exp_cfg.add_section(self.name)
            save_config_file(exp_cfg,exp_cfg_file)
            return True

    def set_config_file(self):
        """set_config_file read the appropriate config file (main-experiment.cfg) and write it on the conf exp dir
        read the init conf file and write a specific one in ${EXP_NAME}/conf/${EXP_NAME}.cfg"""
        if not self.tree:
            raise Exception("Project not yet created")
        conf = pathlib.Path(__file__).parent / "conf/main-session.cfg"
        parser = ConfigParser(interpolation=ExtendedInterpolation())
        with open(conf) as fh:
            parser.read_file(fh)
        parser['Session']['path'] = self.name
        conf_dir = self.set_location_dir('conf')
        if pathlib.Path(conf_dir).is_dir():
            config_file = self.set_location_file("session.cfg",'conf')
            with open(config_file,"w") as config_fh:
                parser.write(config_fh)
        else:
            print("Session {0:s} not yet completed at this path : {1:s}, created first !".format(self.get_name(),str(self.get_str_path())))

    def set_tree(self):
        """set_tree() write to the path the tree path of the session"""
        descr = pathlib.Path(__file__).parent / "conf/wproject-exp-session.yml"
        try:
            with open(descr) as fh:
                try:
                    tree = yaml.load(fh)
                    rename_nested_key(tree, "SESSION_NAME", self.name)
                    dict_to_dir(data=tree, path=self.path)
                    print("Session {0:s} created at {1:s}".format(self.get_name(),self.get_str_path()))
                    self.tree = True
                except Exception as e:
                    print(e)
        except Exception as e:
            print(e)

class Experiment:
    """Class experiment store and write the experiment part"""
    def __init__(self, name, date = None, params = None):
        if not date:
            date = set_default_date()
        if not params:
            params = ''
        else: 
            params = "_" + params
        self.name = name + "_" + date + params
        self.tree = False

    def create(self,project,project_cfg,project_cfg_file):
        self.path = pathlib.Path(project.set_location_dir(project_cfg['Experiments']['path']))
        if self.name in project_cfg.sections():
            self.tree = True
            print("Experiment {} already exist.".format(self.name))
            return False
        else:
            self.set_tree()
            self.set_config_file()
            project_cfg.add_section(self.name)
            save_config_file(project_cfg,project_cfg_file)
            return True

    def set_location_dir(self, path = None, dir = None):
        """get_location_dir return the path join to the root dir of the project and created the directory if not exist. return a str"""
        if path:
            p = pathlib.Path(path)
            # if p.parts[0] == self.name the path start with the project name so just take the path of the project
            if p.parts[0] == self.name:
                p = self.path / p
            elif p.parts[0] == '/':
                raise ValueError("Path can't start with / : {0:s}".format(str(p)))
            else:
                p = self.path / self.name / pathlib.Path(path)
            if not p.exists():
                p.mkdir()
        else:
            p = self.path / self.name
        if dir:
                p = p / dir
                if not p.exists():
                    p.mkdir()
        return str(p)

    def set_location_file(self, file_name, path = None):
        """get_location_file return the path join to the file name, create the directory if not exist and return a string"""
        p = self.set_location_dir(path)
        file_path = pathlib.PurePath(p).joinpath(file_name)
        return str(file_path)

    def get_name(self):
        return(self.name)
    
    def get_str_path(self):
        return str(self.path)

    def set_config_file(self):
        """set_config_file read the appropriate config file (main-expereince.cfg) and write it on the conf exp dir
        read the init conf file and write a specific one in ${EXP_NAME}/conf/${EXP_NAME}.cfg"""
        if not self.tree:
            raise Exception("Project not yet created")
        conf = pathlib.Path(__file__).parent / "conf/main-experiment.cfg"
        parser = ConfigParser(interpolation=ExtendedInterpolation())
        with open(conf) as fh:
            parser.read_file(fh)
        parser['experiment']['path'] = self.name
        conf_dir = self.set_location_dir('conf')
        if pathlib.Path(conf_dir).is_dir():
            config_file = str(self.set_location_file("experiment.cfg",'conf'))
            with open(config_file,"w") as config_fh:
                parser.write(config_fh)
        else:
            print("Experiment {0:s} not yet completed at this path : {1:s}, created first !".format(self.get_name(),str(self.get_str_path())))

    def set_tree(self):
        """set_tree() write to the path the tree of the experiment"""
        descr = pathlib.Path(__file__).parent / "conf/wproject-experiment.yml"
        try:
            with open(descr) as fh:
                try:
                    tree = yaml.load(fh)
                    rename_nested_key(tree, "EXP_NAME", self.name)
                    dict_to_dir(data=tree, path=self.get_str_path())
                    print("Experiment {0:s} created at {1:s}".format(self.get_name(),self.get_str_path()))
                    self.tree = True
                except Exception as e:
                    print(e)
        except Exception as e:
            print(e)

class Project:
    """Class Project store the project itsel"""
    def __init__(self, name, path=None,project_type=None):
        self.name = name
        self.tree = False
        if project_type:
            project_type_list = self._get_list_project_type()
            if project_type not in project_type_list:
                raise Exception("Wrong project_type of project : must be 'wide-project' or 'short-project'.")
            else:
                self.project_type = project_type
        else:
            self.project_type = self._set_default_project_type()

        if path:
            p = pathlib.Path(path)
            if p.is_dir():
                self.path = p.resolve()
            else:
                raise Exception("{} not a regular folder".format(p))

        else:
            self.path = pathlib.Path.cwd().resolve()

    def _get_list_project_type(self):
        return ['wide-project','short-project']

    def _set_default_project_type(self):
        """Return the default project project_type (wide-project)"""
        return "wide-project"

    def set_path(self,path=None):
        if path:
            p = pathlib.Path(path).resolve()
            if p.is_dir():
                self.path = p
            else:
                raise Exception("{} not a regular folder".format(p))
        else:
            self.path = pathlib.Path().cwd().resolve()

    def set_name(self,name):
        self.name = name

    def set_tree_att(self,is_tree):
        self.tree = is_tree
 
    def set_type(self,project_type = None):
        if project_type:
            project_type_list = self._get_list_project_type()
            if project_type in project_type_list:
                self.project_type = project_type
            else:
                raise Exception('Wrong project_type of project, must be "wide-project" or "short-project"')
        else:
            self.project_type = self._set_default_project_type()

    def get_path(self):
        return self.path

    def get_name(self):
        return self.name

    def get_type(self):
        return self.project_type

    def get_current_path(self):
        return pathlib.Path().cwd()
    
    def set_location_dir(self, path = None, dir = None):
        """get_location_dir return the path join to the root dir of the project and created the directory if not exist. return a str"""
        if path:
            p = pathlib.Path(path)
            # if p.parts[0] == self.name the path start with the project name so just take the path of the project
            if p.parts[0] == self.name:
                p = self.path / p
            elif p.parts[0] == '/':
                raise ValueError("Path can't start with / : {0:s}".format(str(p)))
            else:
                p = self.path / self.name / pathlib.Path(path)
            if not p.exists():
                p.mkdir()
        else:
            p = self.path / self.name
        if dir:
                p = p / dir
                if not p.exists():
                    p.mkdir()
        return str(p)

    def set_location_file(self, file_name, path = None):
        """get_location_file return the path join to the file name, create the directory if not exist and return a string"""
        p = self.set_location_dir(path)
        file_path = pathlib.PurePath(p).joinpath(file_name)
        return str(file_path)

    def create(self):
        main_config_file = self.path / self.name / "conf/main-project.cfg"
        if pathlib.Path(main_config_file).exists():
            print("Project tree OK.")
            self.set_tree_att(True)
            return False
        else:
            self.set_tree()
            self.set_config_file()
            return True

    def set_config_file(self):
        """set_config_file read the appropriate config file (wide or short project) and write it on the conf dir of the project 
        read the init conf file and write a specific one in ${PROJECT_NAME}/conf${PROJECT_NAME}.cfg"""
        if not self.tree:
            raise Exception("Project not yet created")
        if self.project_type == "wide-project":
            conf = pathlib.Path(__file__).parent / "conf/main-wide-project.cfg"
        else:
            conf = pathlib.Path(__file__).parent / "conf/main-short-project.cfg"
        parser = ConfigParser(interpolation=ExtendedInterpolation())
        with open(conf) as fh:
            parser.read_file(fh)
        parser['Project']['path'] = self.name
        conf_dir = self.set_location_dir('conf')
        if pathlib.Path(conf_dir).is_dir():
            config_file = str(self.set_location_file("main-project.cfg",'conf'))
            with open(config_file,"w") as config_fh:
                parser.write(config_fh)
        else:
            print("Project {0:s} not yet completed at this path : {1:s}, created first !".format(self.get_name(),str(self.get_path())))


    def set_tree(self):
        """set_tree() write to the path the tree of the project (wide or expl)"""
        if self.project_type == "wide-project":
            descr = str(pathlib.Path(__file__).parent / "conf/wide-project.yml")
        else:
            descr = str(pathlib.Path(__file__).parent / "conf/short-project.yml")
        try:
            with open(descr) as fh:
                try:
                    tree = yaml.load(fh)
                    rename_nested_key(tree, "PROJECT_NAME", self.name)
                    project_name_rp = self.name + ".Rp"
                    rename_nested_key(tree, "PROJECT_NAME.Rp", project_name_rp)
                    path = str(self.path)
                    dict_to_dir(data=tree, path=path)
                    print("{0:s} created at {1:s}".format(self.project_type, str(self.path)))
                    self.tree = True
                except Exception as e:
                    print(e)
        except Exception as e:
            print(e)

def set_default_date():
    now = datetime.now()
    return now.strftime('%Y-%m-%d')

def set_default_moment():
    now = datetime.now()
    return now.strftime('%Y-%m-%d_%H-%M')

def rename_nested_key(dictionnary, old_key, new_key):
    """rename_nested_key remane a key in a nested dictionnary"""
    if isinstance(dictionnary, dict):
        if old_key in dictionnary.keys():
            try:
                dictionnary[new_key] = dictionnary.pop(old_key)
            except KeyError as ex:
                raise Exception("This key ({}) is not present is the structure. {}".format(old_key,ex))
        else:

            for element in dictionnary:
                if isinstance(dictionnary[element], dict):
                    rename_nested_key(dictionnary[element], old_key, new_key)
                elif isinstance(dictionnary[element], list):
                    rename_nested_key(dictionnary[element], old_key, new_key)
    elif isinstance(dictionnary, list):
        for element in dictionnary:
            if isinstance(element, dict):
                rename_nested_key(element, old_key, new_key)

def dict_to_dir(data, path=str()):
    """dict_to_dir expects data to be a dictionary with one top-level key."""

    dest = os.path.join(os.getcwd(), path)

    if isinstance(data, dict):
        for key, value in data.items():
            try:
                os.makedirs(os.path.join(dest, key))
            except FileExistsError as fee:
                print(fee)
            dict_to_dir(value, os.path.join(path, str(key)))

    elif isinstance(data, list):
        for i in data:
            if isinstance(i, dict):
                dict_to_dir(i, path)
            else:
                with open(os.path.join(dest, i), "a"):
                    os.utime(os.path.join(dest, i), None)


    if isinstance(data, dict):
        return list(data.keys())[0]

def get_config_parser(cfg_file):
    """get config parser take a config file and return the config parser of this file"""
    config = ConfigParser(interpolation=ExtendedInterpolation())
    try:
        with open(cfg_file) as fh:
            config.read_file(fh)
    except FileNotFoundError as ee:
        raise Exception(ee)
    return config

def save_config_file(config,cfg_file):
    try:
        with open(cfg_file,"w") as config_file:
            config.write(config_file)
    except FileNotFoundError as ee:
        raise Exception(ee)
