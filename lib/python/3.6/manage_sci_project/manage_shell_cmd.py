#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import errno
import subprocess
from shlex import split

def silent_remove(filename):
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred

def run_shell_command_pipe_legacy(shell_command):
    """
    run shell command with pipe in shell style and return proper output
    """
    process = subprocess.Popen(shell_command, shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (out, err) = process.communicate()
    if out:
        return(out.decode("utf-8"))
    if err:
        return(err.decode("utf-8"))

def run_shell_command_legacy(shell_command):
    shell_command = split(shell_command)
    process = subprocess.Popen(shell_command, shell = False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (out, err) = process.communicate()
    if out:
        return(out.decode("utf-8"))
    if err:
        return(err.decode("utf-8"))

def run_secure_shell_command(shell_command):
    """run_secure_shell_command run a secure shell with or without pipe (|) and with or without redirection (>: write,>>: append). 
    It run a secure shell as the option shell is False and the command is splitted into a list."""
    redirection_count = shell_command.count('>')
    if redirection_count == 0:
        redirection = ''
    elif redirection_count == 1:
        redirection = "w"
        shell_command_list = shell_command.split('>')
        if len(shell_command_list) != 2:
            raise ValueError("Too many redirection pipe")
        else:
            shell_command = shell_command_list[0].strip()
            file_out = shell_command_list[1].strip()
    elif redirection_count == 2:
        redirection = "a"
        shell_command_list = shell_command.split('>>')
        if len(shell_command_list) != 2:
            raise ValueError("Too many redirection pipe")
        else:
            shell_command = shell_command_list[0].strip()
            file_out = shell_command_list[1].strip()
    else:
        raise ValueError("Wrong number of redirection pipe (>)")
    if '|' in shell_command:
        shell_commands = shell_command.split('|')
        for i,shell_cmd in enumerate(shell_commands):
            shell_cmd = split(shell_cmd.strip())
            # if i = 0 first cmd
            if i == 0:
                process_tmp = subprocess.Popen(shell_cmd, shell = False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            else:# if i = len(cmds) - 1 :  last cmd -> redirection to the file out (redirection define the type Write (>) or Append (>>))
                if redirection and i == (len(shell_commands) - 1):
                    with open(file_out,redirection) as fh:
                        process_tmp = subprocess.Popen(shell_cmd, shell = False, stdin=process_tmp.stdout,stdout=fh, stderr=subprocess.PIPE)
                        try:
                            process_tmp.communicate()
                            return
                        except Exception as e:
                            raise ValueError("Can't open file", e)
                else:
                    process_tmp = subprocess.Popen(shell_cmd, shell = False, stdin=process_tmp.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)   
        (out, err) = process_tmp.communicate()
        if out:
            return(out.decode("utf-8"))
        if err:
            return(err.decode("utf-8"))

    else:
        shell_command = split(shell_command.strip())
        if redirection:
            with open(file_out,redirection) as fh:
                process = subprocess.Popen(shell_command, shell = False, stdin=subprocess.PIPE,stdout=fh, stderr=subprocess.PIPE)
                try:
                    process.communicate()
                except Exception as e:
                    raise ValueError("Can't open file", e)
        else:
            process = subprocess.Popen(shell_command, shell = False, stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
            (out, err) = process.communicate()
            if out:
                return(out.decode("utf-8"))
            if err:
                return(err.decode("utf-8"))
