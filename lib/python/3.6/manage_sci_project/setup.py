from setuptools import setup

setup(name='manage_sci_project',
      version='0.1',
      description='manage_sci_project designed to manage a scientific project in a Jupyter Notebook',
      url='https://framagit.org/arnome/managesciproject',
      author='Arnaud Mounier',
      author_email='arnaud.mounier@inra.fr',
      license='CECILL',

      packages=['manage_sci_project'],
      zip_safe=False,
      include_package_data=True)
